from simple_crud.database.models.database_models import *
from simple_crud.utilities.jwt_token_utilities import decode_token, encode_token
from typing import Union


def get_user_from_uid(uid: str) -> User:
    return User.query.get(uid)


def get_user_from_email(email: str) -> User:
    return User.query.filter(
        User.email == email
    ).first()


def get_user_from_username(username: str) -> User:
    return User.query.filter(
        User.username == username
    ).first()


def check_user_credentials(username: str, password: str) -> bool:
    user = get_user_from_username(username)
    return user.check_password(password)


def create_new_user(username, email, password, **kwargs) -> Union[User, str]:
    existing_username, existing_email = get_user_from_username(username), get_user_from_email(email)
    if any(item is not None for item in [existing_email, existing_username]):
        return "Username/Email is already registered"
    user = User(username, email, password, **kwargs)
    db.session.add(user)
    db.session.commit()
    return user


def get_user_from_token(token: str) -> User:
    decoded_data, err = decode_token(token)
    return get_user_from_uid(decoded_data.get('uid'))


def update_user(uid: str, **kwargs):
    user = get_user_from_uid(uid)
    return user.update(**kwargs)


def generate_token_for_user(uid: str) -> str:
    return encode_token({"uid": uid})

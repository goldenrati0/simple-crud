from simple_crud.database.app_db import app
from simple_crud.blueprints.user.routes import userbp
from simple_crud.blueprints.product.routes import productbp
from flask import jsonify


@app.route("/")
def home():
    return jsonify({
        "msg": "Welcome"
    })


app.register_blueprint(userbp, url_prefix="/api/v1")
app.register_blueprint(productbp, url_prefix="/api/v1")

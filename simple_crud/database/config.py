import os

SECRET_KEY = os.environ.get("SECRET_KEY", "secret_key")
SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI',
                                         "postgres://postgres:postgres@127.0.0.1:5432/simple_crud")
SQLALCHEMY_TRACK_MODIFICATIONS = False

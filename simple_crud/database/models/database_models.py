from datetime import datetime
from uuid import uuid4

from flask_bcrypt import Bcrypt
from sqlalchemy import String
from sqlalchemy.dialects.postgresql import UUID, ARRAY

from simple_crud.database.app_db import db, app

bcrypt = Bcrypt(app)


def generate_uuid():
    return uuid4().hex


purchase_history = db.Table('purchase_history',
                            db.Column('product_uid', UUID, db.ForeignKey('product.uid')),
                            db.Column('user_uid', UUID, db.ForeignKey('user.uid')))


class User(db.Model):
    __tablename__ = "user"
    uid = db.Column(UUID, primary_key=True, default=generate_uuid)
    username = db.Column(db.String(100), unique=True, index=True, nullable=False)
    email = db.Column(db.String(100), unique=True, index=True, nullable=False)
    password = db.Column(db.String(255), nullable=False)
    joined = db.Column(db.DateTime, default=datetime.utcnow)
    # Relationships
    address = db.relationship('UserAddress', uselist=False, cascade='all, delete-orphan', back_populates="user",
                              lazy=True)  # one-to-one
    products = db.relationship("Product", cascade="all, delete-orphan", back_populates="owner",
                               lazy=True)  # one-to-many
    purchases = db.relationship("Product", secondary=purchase_history, back_populates="buyers",
                                lazy="dynamic")  # many-to-many

    def __init__(self, username, email, password, **kwargs):
        super(User, self).__init__(**kwargs)
        self.username = username
        self.email = email
        self.password = bcrypt.generate_password_hash(password, rounds=12).decode("utf-8")

    def update(self, **kwargs):
        self.username = kwargs.get('username') or self.username
        self.email = kwargs.get('email') or self.email
        self.password = kwargs.get('password') or self.password
        self.address = UserAddress(**kwargs.get('address')) or self.address

        try:
            db.session.commit()
            return self, False
        except Exception as ex:
            return str(ex), True

    def check_password(self, password):
        return bcrypt.check_password_hash(self.password, password)

    def add_address(self, **kwargs):
        self.address = UserAddress(**kwargs)

    def get_address(self):
        return self.address.to_json() if self.address else None

    def get_products(self):
        return [product.to_json() for product in self.products]

    def get_purchases(self):
        return [purchase.to_json() for purchase in self.purchases]

    def profile_info(self):
        return {
            "uid": self.uid,
            "username": self.username,
            "address": self.get_address()
        }

    def to_json(self):
        return {
            "uid": self.uid,
            "username": self.username,
            "email": self.email,
            "joined": str(self.joined),
            "address": self.get_address(),
            "attributes": [
                "products",
                "purchases"
            ]
        }


class UserAddress(db.Model):
    __tablename__ = "user_address"
    uid = db.Column(UUID, primary_key=True, default=generate_uuid)
    user_uid = db.Column(UUID, db.ForeignKey("user.uid"))
    road_no = db.Column(db.String(20))
    area = db.Column(db.String(100))
    city = db.Column(db.String(100))
    zip_code = db.Column(db.String(20))
    country = db.Column(db.String(100))
    # Relationships
    user = db.relationship("User", back_populates="address", lazy=True)

    def to_json(self):
        return {
            "road_no": self.road_no,
            "area": self.area,
            "city": self.city,
            "zip_code": self.zip_code,
            "country": self.country
        }


class Product(db.Model):
    __tablename__ = "product"
    uid = db.Column(UUID, primary_key=True, default=generate_uuid)
    user_uid = db.Column(UUID, db.ForeignKey("user.uid"))
    name = db.Column(db.String(255), nullable=False)
    description = db.Column(db.Text)
    posted_at = db.Column(db.DateTime, default=datetime.utcnow)
    price = db.Column(db.Float, default=0.0)
    images = db.Column(ARRAY(String))
    # Relationships
    owner = db.relationship("User", back_populates="products", lazy=True)
    buyers = db.relationship("User", secondary=purchase_history, back_populates="purchases", lazy="dynamic")

    def __eq__(self, other):
        return self.uid == other.uid \
               and self.user_uid == other.user_uid \
               and self.price == other.price \
               and self.name == other.name

    def get_buyers(self):
        return [buyer.profile_info() for buyer in self.buyers]

    def to_json(self):
        return {
            "uid": self.uid,
            "name": self.name,
            "description": self.description,
            "posted_at": str(self.posted_at),
            "price": self.price,
            "images": self.images,
            "owner": self.owner.profile_info()
        }

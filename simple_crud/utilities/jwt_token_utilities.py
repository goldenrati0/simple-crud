import os
from datetime import datetime, timedelta

import jwt
from typing import Tuple

JWT_KEY = os.environ.get("JWT_KEY", "jwt_secret_ley")
JWT_ALGORITHM = os.environ.get("JWT_ALGORITHM", "HS512")


def encode_token(data: dict) -> str:
    data['iat'] = datetime.utcnow()
    data['nbf'] = datetime.utcnow()
    data['exp'] = datetime.utcnow() + timedelta(hours=12)
    token = jwt.encode(payload=data, key=JWT_KEY, algorithm=JWT_ALGORITHM)
    return token.decode("utf-8")


def decode_token(token: str) -> Tuple[dict, bool]:
    try:
        decoded_dict = jwt.decode(jwt=token, key=JWT_KEY, algorithms=[JWT_ALGORITHM])
        return decoded_dict, False
    except Exception as ex:
        return str(ex), True


if __name__ == '__main__':
    token = encode_token({"uid": "test-uid"})
    print(token)
    data, err = decode_token(token)
    print(data)
